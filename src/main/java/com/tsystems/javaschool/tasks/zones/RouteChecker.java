package com.tsystems.javaschool.tasks.zones;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RouteChecker {

    private HashMap<Integer, Zone> zones;

    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     * <p>
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     * - A has link to B
     * - OR B has a link to A
     * - OR both of them have a link to each other
     *
     * @param zoneState        current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds) {

        zones = new HashMap<>();
        for (Zone zone : zoneState) {
            zones.put(zone.getId(), zone);
        }

        if (requestedZoneIds.size() == 1) {
            return zones.containsKey(requestedZoneIds.get(0));
        }

        ArrayList<Integer> hub = new ArrayList<>();
        ArrayList<Integer> rest = new ArrayList<>(requestedZoneIds);
        hub.add(rest.remove(0));

        for (int i = 0; i < hub.size(); i++) {
            int id = hub.get(i);
            for (int j = 0; j < rest.size(); j++) {
                int idRest = rest.get(j);
                if (isConnected(id, idRest)) {
                    hub.add(idRest);
                    rest.remove(j--);
                }
            }
            if (rest.isEmpty()) {
                break;
            }
        }

        return rest.isEmpty();
    }

    private boolean isConnected(Integer id1, Integer id2) {
        Zone zone1 = zones.get(id1);
        Zone zone2 = zones.get(id2);
        boolean result = zone1 != null && zone2 != null;
        result = result && zone1.getNeighbours().contains(id2);
        return result || zone2.getNeighbours().contains(id1);
    }
}
