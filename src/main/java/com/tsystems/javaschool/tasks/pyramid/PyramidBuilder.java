package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        double pyramidHeight = getPyramidHeight(inputNumbers.size());
        if (pyramidHeight % 1 != 0 || pyramidHeight == 0 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(null);
        ListIterator<Integer> iterator = inputNumbers.listIterator();

        int countRow = (int) pyramidHeight;
        int[][] pyramid = new int[countRow][2 * countRow - 1];

        for (int row = 0; row < countRow; row++) {
            int column = countRow - 1 - row;

            for (int k = 0; k <= row; k++) {
                pyramid[row][column] = iterator.next();
                column += 2;
            }
        }

        return pyramid;
    }

    private double getPyramidHeight(int countNumbers) {
        return (Math.sqrt(8 * countNumbers + 1) - 1) / 2;
    }
}